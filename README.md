# build-tools

This repo contains build tools for vuejs projects. It's based on a docker containter so almost nothing is going to be installed in your PC.
Of course, in order to use it you will need to first have docker and docker-composer installed on your system.

# Usage
To use this build system to start a vuejs project from scratch you need to follow this steps

- Create your project directory
```bash
$ mkdir my-vue-app
$ cd my-vue-app
```

- Clone this repo into the project directory
```bash
$ git clone git@gitlab.com:web100/spa/vuejs/build-tools.git
```

- Source the envsetup.sh script (mid the space between the dot and the dash)
```bash
$ . /build-tools/envsetup.sh
```

- Create you app skeleton
```bash
$ create-vue-app
```
Note: `create-vue-app` is equivalent to run the standard `vue create` command.

If everything went well, after completing the process you will have your new project created in your local filesystem

# Serving your app
To debug or serve your app you need to use the following command
```bash
$ serve
```
You can now access to your application going to `localhost:8080`





