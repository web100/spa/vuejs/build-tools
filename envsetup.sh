#!/bin/bash

function gettop {
	local TOPFILE=build-tools/envsetup.sh
	if [ -f $TOPFILE ] ; then
		PWD= /bin/pwd
	else
		local HERE=$PWD
		local T=
		while [ \( ! \( -f $TOPFILE \) \) -a \( $PWD != "/" \) ] ; do
			\cd ..
			T=`PWD= /bin/pwd -P`
		done
		\cd $HERE
		if [ -f "$T/$TOPFILE" ] ; then
			echo $T
		fi
	fi

}

function container-rebuild {
    local T=$(gettop)
    if [ ! "$T" ] ; then
        echo "Coudn't locate the top of the directory tree."
        return
    fi

    export HOST_USER_ID=$(id -u)
    export HOST_GROUP_ID=$(id -g)
    export SRC_DIR=$T
    export INIT_CMD="npm -v"

    echo "Starting build environment..."
    docker-compose -f "$T/build-tools/docker/docker-compose.yml" build

}


function serve-prod {
    local T=$(gettop)
    if [ ! "$T" ] ; then
        echo "Coudn't locate the top of the directory tree."
        return
    fi

    export HOST_USER_ID=$(id -u)
    export HOST_GROUP_ID=$(id -g)
    export SRC_DIR=$T
    export INIT_CMD="npm run serve  dist"

    echo "Starting build environment..."
    docker-compose -f "$T/build-tools/docker/docker-compose.yml" up --no-deps

}

function build {
    local T=$(gettop)
    if [ ! "$T" ] ; then
        echo "Coudn't locate the top of the directory tree."
        return
    fi

    export HOST_USER_ID=$(id -u)
    export HOST_GROUP_ID=$(id -g)
    export SRC_DIR=$T
    export INIT_CMD="npm run build"

    echo "Starting build environment..."
    docker-compose -f "$T/build-tools/docker/docker-compose.yml" up --no-deps

}

function serve {
    local T=$(gettop)
    if [ ! "$T" ] ; then
        echo "Coudn't locate the top of the directory tree."
        return
    fi

    export HOST_USER_ID=$(id -u)
    export HOST_GROUP_ID=$(id -g)
    export SRC_DIR=$T
    export INIT_CMD="npm run serve"

    echo "Starting build environment..."
    docker-compose -f "$T/build-tools/docker/docker-compose.yml" up --no-deps
}

function create-vue-app {
    local T=$(gettop)
    if [ ! "$T" ] ; then
        echo "Coudn't locate the top of the directory tree."
        return
    fi

    export HOST_USER_ID=$(id -u)
    export HOST_GROUP_ID=$(id -g)
    export SRC_DIR=$T
    export INIT_CMD='sh -c "vue create new-app && cp -R new-app/* . && rm -rf new-app"'

    echo "Starting build environment..."
    docker-compose -f "$T/build-tools/docker/docker-compose.yml" run --rm app
}

function vuecli {
    local T=$(gettop)
    local ARGS="$@"

    if [ ! "$T" ] ; then
        echo "Coudn't locate the top of the directory tree."
        return
    fi

    export HOST_USER_ID=$(id -u)
    export HOST_GROUP_ID=$(id -g)
    export SRC_DIR=$T
    export INIT_CMD="vue $ARGS"

    echo "Starting build environment..."
    docker-compose -f "$T/build-tools/docker/docker-compose.yml" run --rm  app
}


function npm-run {
    local T=$(gettop)
    local ARGS="$@"

    if [ ! "$T" ] ; then
        echo "Coudn't locate the top of the directory tree."
        return
    fi

    export HOST_USER_ID=$(id -u)
    export HOST_GROUP_ID=$(id -g)
    export SRC_DIR=$T
    export INIT_CMD="npm $ARGS"

    echo "Starting build environment..."
    docker-compose -f "$T/build-tools/docker/docker-compose.yml" run --rm --service-ports app

}
